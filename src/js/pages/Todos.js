import React from "react";

import Todo from "../components/Todo";
import * as TodoActions from "../actions/TodoActions";
import TodoStore from "../stores/TodoStore";


export default class Todos extends React.Component {
  constructor() {
    super();
    this.getTodos = this.getTodos.bind(this);
    this.state = {
      todos: TodoStore.getAll(),
      title: "Welcome",
    };
  }

  componentWillMount() {
    TodoStore.on("change", this.getTodos);
  }

  componentWillUnmount() {
    TodoStore.removeListener("change", this.getTodos);
  }

  getTodos() {
    this.setState({
      todos: TodoStore.getAll(),
    });
  }



  reloadTodos() {
    TodoActions.reloadTodos();
  }


  changeTitle(title) {
    this.setState({title});
  }

  handleSubmit(event){
    this.setState({display: this.state.value});
    this.state.value = '';
    let title = document.getElementById('title'),
        content = document.getElementById('content');
    TodoStore.createTodo(title, content);
    this.reloadTodos();
    event.preventDefault();

  }

  handleChange(event){
    this.setState({value: event.target.value});

  }


  render() {
    const { todos } = this.state;

    const TodoComponents = todos.map((todo) => {
        return <Todo key={todo.id} {...todo}/>;
    });

    return (
        <div>

            <button classonClick={this.reloadTodos.bind(this)}>Reload!</button>
            <form onSubmit={this.handleSubmit.bind(this)}>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Enter la nouvelle tâche:</span>
                    </div>
                    <input id="title" class="form-control" aria-label="Saisir le titre"></input>
                    <input id="content" class="form-control" aria-label="Saisir la description"></input>
                </div>
                <input type="submit" value="Submit" />
            </form>
            <h1>Tâches</h1>

            <ul>{TodoComponents}</ul>

        </div>


    );
  }
}
