const MOCKS_DATA = {
    "init": [
      {
        "id": 100,
        "text": "mock_1",
        "complete": false
      },
      {
        "id": 101,
        "text": "mock_2",
        "complete": false
      }
    ],
    "reload": [
      {
        "id": 102,
        "text": "mock_3",
        "complete": false
      },
      {
        "id": 103,
        "text": "mock_3",
        "complete": false
      }
    ]
  };

export default MOCKS_DATA;
