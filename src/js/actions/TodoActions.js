import dispatcher from "../dispatcher";
import axios from 'axios';



import MOCKS_DATA from '../mocks/data';

export function createTodo(text) {
  dispatcher.dispatch({
    type: "CREATE_TODO",
    text,
  });
}

export function deleteTodo(id) {
    axios.delete('http://localhost:8081/api/v1/task/'+id)
        .then(function(res) {
            console.log(res);
        })
}

export function reloadTodos() {
   axios.get('http://localhost:8081/api/v1/tasks')
     .then(function(res) {

       let test = [];

       for (let todo of res.data)
        test.push({id: todo._id, text: todo.title, createdAt: todo.createdAt, complete: true});

       dispatcher.dispatch( {type: "RECEIVE_TODOS", todos: test} );
     })
     .catch(err =>  console.error(err));
}

