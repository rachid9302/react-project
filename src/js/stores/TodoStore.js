import { EventEmitter } from "events";

import dispatcher from "../dispatcher";
import axios from "axios/index";

class TodoStore extends EventEmitter {
    constructor() {
        super()
        this.todos = [];

        axios.get('http://localhost:8081/api/v1/tasks')
            .then(function (res) {

                let test = [];

                for (let todo of res.data)
                    test.push({id: todo._id, text: todo.title, createdAt: todo.createdAt, complete: true});

                dispatcher.dispatch({type: "RECEIVE_TODOS", todos: test});
            })
            .catch(err => console.error(err));
    }

    createTodo(title, content) {
        axios.post('http://localhost:8081/api/v1/tasks', {
            title: title.value,
            content: content.value
        })
        .then(function (res) {

        })
        .catch(err => console.error(err));
            this.emit("change");


    }


  getAll() {
    return this.todos;
  }

  handleActions(action) {
    switch(action.type) {
      case "CREATE_TODO": {
        this.createTodo(action.text);
        break;
      }
      case "RECEIVE_TODOS": {
        this.todos = action.todos;
        this.emit("change");
        break;
      }
    }
  }

}

const todoStore = new TodoStore;
dispatcher.register(todoStore.handleActions.bind(todoStore));

export default todoStore;
